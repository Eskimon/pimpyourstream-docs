---
home: true
heroImage: /hero.png
actionText: Get Started →
actionLink: /guide/
heroText: PimpYour.Stream
tagline: Your ultimate asset-display broadcasting platform
features:
- title: Your ultimate asset-display broadcasting platform
  details: Treat your audience with professional dynamic overlays to add value to your broadcast. Update them easily and instantly to stay on top of the information! PimpYour.Stream is a flexible solution that allows you to design and operate custom assets, tailored to your content and needs. Add text, pictures, videos, HTML elements, timers and combinations of those to create your own custom elements.
- title: Perfect for esports!
  details: Design complex assets with brackets, standings, schedules, and update them in real time with a few clicks. Easily add teams, with their names and logos and watch them magically fit your scenes and be shown to your audience. Forget about expensive softwares and spreadsheets to display dynamic graphical assets in your live broadcasts. PimpYour.Stream has got everything you need, under your control, for fully customized and professional-looking esports events!
- title: Easy to setup, easy to operate
  details: There's nothing to install to use PimpYour.Stream, it's all online! Create your scenes, fill them with elements, and integrate them in your broadcasting software as an HTML browser source. Each update of your scenes is instantly visible in your broadcasts. This allows to operate your scenes remotely and dispatch your production efforts without caring about logistics!
footer: Copyright © 2021-present pimpyour.stream
---

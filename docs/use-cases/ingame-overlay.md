# Create an ingame overlay

## Preliminary steps:

- Upload your custom fonts
- Create a Teams template with a Text element (TEAM NAME) and a Picture element (TEAM LOGO)
- Create some Template Instances for the Teams template
- Create a scene

## Part 1 : background and teams

NB: We recommend you find a way to classify elements (a way for you to remember that the elements belong to that particular scene), using categories (assigning your scene and elements with the same category) or element names (using an ID as a prefix).

- Create a picture/video element for the background.
- If you have different picture/video files for each BO type (BO3 / BO5 / BO7 / ...), upload them all as single elements, then create a picker for picture/video elements that fetches the elements you uploaded (sorting them using categories or element names).
- Create two template placers for Teams template.
- Add the background picture/video/picker element scene.
- Add both template placers to the scene.
- Place the text & picture elements of both placers, using the edit button.

## Part 2: scores

- Create a picture element for all the scores of each of the sides. Use a blank PNG for score 0.
- Create two pickers, one for each side, and use the filters to select the right score picture elements.
- Add the two pickers to the scene.

## Part 3: additional elements

- If you need additional text information, create a text element, customize it with the font, size, color..., add it to the scene then place it.
- If you need additional picture/video, just use your broadcasting software. Just kidding, you can add them in your scene as well, as picture or video elements.

## Final steps

- Copy the scene client url which is on the top right of your screen
- Paste it in the URL of a browser source in your broadcasting software, along with its dimensions (1920x1080 by default)
- And voilà!

# Create a bracket

## Preliminary steps:

- Upload your custom fonts
- Create a Teams template with a Text element (TEAM NAME) and a Picture element (TEAM LOGO)
- Create some Template Instances for the Teams template
- Create a scene

## Part 1: background and teams

NB: We recommend you find a way to classify elements (a way for you to remember that the elements belong to that particular scene), using categories (assigning your scene and elements with the same category) or element names (using an ID as a prefix).

- Create a picture/video element for the background.
- Create a template placer for Teams template.
- Create a text element for the score
- Add the background picture/video, the template placer and the text element to the scene.
- Place the text & picture elements of the placer as well as the text element, using the edit button.
- After they have been correctly configured, duplicate the template placer and the text element for as many times as needed, according to the asset.
- Add the duplicated elements to the scene and place them.

## Part 2: additional elements


- If you need additional text information, create a text element, customize it with the font, size, color..., add it to the scene then place it.
- If you need additional picture/video, just use your broadcasting software. Just kidding, you can add them in your scene as well, as picture or video elements.

## Final steps

- Copy the scene client url which is on the top right of your screen
- Paste it in the URL of a browser source in your broadcasting software, along with its dimensions (1920x1080 by default)
- And voilà!

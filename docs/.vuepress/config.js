module.exports = {
  title: 'Docs@PimpYour.Stream',
  description: 'Docs@PimpYour.Stream',
  base: '/',
  dest: 'public',
  themeConfig: {
    nav: [
      { text: 'Home', link: '/' },
      { text: 'Guide', link: '/guide/' },
      { text: 'Use cases', link: '/use-cases/' },
    ],
    lastUpdated: 'Last Updated',
    displayAllHeaders: true,
    sidebar: {
      '/guide/': [
        ['', 'Guide'],
        ['elements', 'Elements'],
        ['scenes', 'Scenes'],
        ['tips', 'Tips'],
      ],
      '/use-cases/': [
        ['', 'Use Cases'],
        ['bracket', 'Bracket'],
        ['ingame-overlay', 'Ingame Overlay'],
      ],
    }
  }
}

# In short

- PYS is entirely operated from a web browser. Therefore, it can also be operated remotely: you don’t have to be at the production site to update broadcast information.
- PYS displays HTML scenes composed of an unlimited number of pictures, short video loops or text elements.
- Create elements and place them in your scenes.
- Have them appear, hide them or change them in two clicks to update your broadcast information instantly.

# Navigation

- Scenes are listed in the left panel of the PYS interface.
- The elements library is displayed in the main screen. It’s sorted by element types.
